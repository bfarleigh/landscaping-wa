<?php
/**
 * Front Page Template
 *
 * @package LandscapingWA
 * @subpackage Landscaping_WA
 * @since Landscaping WA 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

			<?php if ( have_posts() ) : ?>

				<?php landscapingwa_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', get_post_format() ); ?>
       
				<?php endwhile; ?>

				<?php landscapingwa_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'landscapingwa' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'landscapingwa' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>
<script>
  // Load the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replace the 'video' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('video', {
      height: '594',
      width: '780',
      videoId: 'JDWSa2cNb2w',
		playerVars: {
			modestbranding: 1,
			wmode: "opaque",
			autohide: 1,
			showinfo: 0,
			controls: 0,
			autoplay: 1,
			rel: 0
		},
	  events: {
		'onReady': onPlayerReady, //run this fucntion when the video is ready
		'onStateChange': onPlayerStateChange //Run this function when there's a change to the video
	  }
    });
  }
   //  The API will call this function when the video player is ready.
      function onPlayerReady(event) {
		event.target.playVideo();
      }
  // when video ends
	function onPlayerStateChange(event) {    
	if(event.data === 0) {    // on finish    
		$("#video").hide();
		$("#page").show();
	}
	}
</script>
<?php get_footer(); ?>