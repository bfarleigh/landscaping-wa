<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">

			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				if ( ! is_404() )
					get_sidebar( 'footer' );
			?>
 
			<div id="site-generator">
		
			</div>
            
            	<nav id="access" role="navigation">
				<h3 class="assistive-text"><?php _e( 'Main menu', 'twentyeleven' ); ?></h3>
				<?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff. */ ?>
				<div class="skip-link"><a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to primary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to primary content', 'twentyeleven' ); ?></a></div>
				<div class="skip-link"><a class="assistive-text" href="#secondary" title="<?php esc_attr_e( 'Skip to secondary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to secondary content', 'twentyeleven' ); ?></a></div>
				<?php /* Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assigned to the primary location is the one used. If one isn't assigned, the menu with the lowest ID is used. */ ?>
				<?php //wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                
    <div class="menu-main-container" style="text-align:center">
        <ul class="menu" id="menu-main">
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33" id="menu-item-33" style="padding-top:20px;">
	            <a href="http://www.landscapingwa.com.au/water/">
                <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/waterbutton.jpg" /><br /><br />
                Water
                </a>
            </li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/timber/">
                <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/timberbutton.jpg" /><br /><br />
                Timber</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/stone/">
            	<img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/stonebutton.jpg" /><br /><br />
                Stone</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/recreational/">
	            <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/recreationalbutton.jpg" /><br /><br />
                Recreational</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/pavements/">
	            <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/pavementsbutton.jpg" /><br /><br />
                Pavements</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/botanic/">
				<img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/botanicbutton.jpg" /><br /><br />
                Botanic</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/design/">
                <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/designbutton.jpg" /><br /><br />
                Design</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36" id="menu-item-36" style="padding-top:20px;">
            	<a href="http://www.landscapingwa.com.au/walk-through/">
                <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/walkthroughbutton.jpg" /><br /><br />
                Walk-through</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26" style="padding-top:20px;">
		      	<a href="http://www.landscapingwa.com.au/contact-us/">
	            <img src="http://www.landscapingwa.com.au/wp-content/themes/landscapingwa/images/buttons/profilebutton.jpg" /><br /><br />
                Contact Us</a></li>
        </ul>
    </div>
			</nav><!-- #access -->
	</footer><!-- #colophon -->
</div><!-- #page -->
</body>
</html>  