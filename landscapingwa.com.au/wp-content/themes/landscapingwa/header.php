<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package LandscapingWA
 * @subpackage Landscaping_WA
 * @since Landscaping WA 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/jquery.jscrollpane.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/lightbox/css/lightbox.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script src="<?php echo get_template_directory_uri(); ?>/lightbox/js/jquery-1.7.2.min.js" type="text/javascript"></script>

<!-- Mouse wheel JavaScript Class -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel.js" type="text/javascript"></script>
<!-- JScrollPane JavaScript Class -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.jscrollpane.min.js" type="text/javascript"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/browserselect.js" type="text/javascript"></script>

<script src="<?php echo get_template_directory_uri(); ?>/lightbox/js/lightbox.js" type="text/javascript"></script>

<script type="text/javascript" id="sourcecode">
	$(function()
	{
		$(".scroll-pane").jScrollPane();
	});
</script>
<noscript>
<style>
/* If we don't have javascript then we need to use css to show the homepage content and hide the video div*/
.home #page{ 
	display:block;
}
.home #video{display:none;}
</style>
</noscript>
</head>

<body <?php body_class(); ?>>
<div id="video"></div>
<div id="page" class="hfeed">
	<header id="branding" role="banner">
    	<div id="logo">
	        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php bloginfo('template_directory');?>/images/logo.png" height="75" width="244">
            </a>
        </div>
        <div id="info">
        	<?php if(is_front_page()):?>
        	<p style="padding-left:220px;font-size:16px;line-height:20px;">
				bryson@landscapingwa.com.au<br/>     
				0403 050 002 	
            </p>
            <?php endif; ?>
        </div>
		
		<div id="images">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php echo get_the_post_thumbnail( $post->ID, array(200), 'post-thumbnail' ); ?>
			</a>
		</div> <!-- header images -->
	</header><!-- #branding -->
	<div id="main">