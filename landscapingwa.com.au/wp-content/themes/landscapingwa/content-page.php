<?php
/**
 * @package LandscapingWA
 * @subpackage Landscaping_WA
 * @since Landscaping WA 1.0
 */
?>
<div id="page_title">  
	<header class="entry-header" style="padding-top:20px">
		<center>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</center>
	</header><!-- .entry-header -->
</div>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'landscapingwa' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

